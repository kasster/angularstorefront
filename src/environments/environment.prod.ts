export const environment = {
  production: true,
  apiHost: 'https://http--forward--yq8v5b646h8r.code.run/',
  apiPort: '',
  shopApiPath: 'shop-api',
  baseHref: '/',
  tokenMethod: 'bearer',
};
